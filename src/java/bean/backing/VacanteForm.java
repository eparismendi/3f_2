package bean.backing;

import bean.models.Estudiante;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.inject.Inject;

@Named(value = "vacanteForm")
@Dependent
public class VacanteForm {

    public VacanteForm() {
    }

    @Inject
    private Estudiante estudiante;

    
 

    private String candidato;

    
  
    public Estudiante getCandidato() {
        return estudiante;
    }

    public void setCandidato(Estudiante estudiante) {
        this.estudiante = estudiante;

    }

    public String enviar() {
        if (this.estudiante.getNombre().equals("Juan")
                && this.estudiante.getContrasena().equals("123")) {

            return "exito";//exito.xhtml

        } else {

            return "fallo";

        }
    }
}
